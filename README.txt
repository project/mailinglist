
Description
-----------

This module lets  drupal handle the creation, subscription and administration of the mailing list. 

The result 
	* mailing list can be created at Create Content -> mailist.
	* enable the block at administer -> blocks -> Mailing lIst.
	

Requirements
------------

This module requires Drupal 4.7 or a later version.


Installation
------------

1) Copy/upload the folders(mailist, sublist, email) to the modules/ directory of your
   Drupal installation.
2) Enable the modules in Drupal (administer -> modules).


Administration
-------------

Compose mails and view subscribers at administer -> mailist


Author
-------
ekgaon technologies pvt ltd <sasi@ekgaon.com>


